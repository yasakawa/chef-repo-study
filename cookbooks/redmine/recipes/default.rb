#
# Cookbook Name:: redmine
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

#==========================================================
# Install prerequisites packages
#==========================================================
# Install development tools
bash "yum groupinstall Development Libraries" do
  user "root"
  group "root"
  code <<-EOC
    yum groupinstall "Development Libraries" -y
  EOC
  not_if "yum grouplist installed | grep 'Development Libraries'"
end

# Install prerequisites for Ruby and Passenger
%w(
  openssl-devel
  readline-devel
  zlib-devel
  curl-devel
  libyaml-devel
  libffi-devel
).each do |pkg|
  package pkg do
    action :install
  end
end

# Install MySQL
%w(
  mysql-server
  mysql-devel
).each do |pkg|
  package pkg do
    action :install
  end
end

# Install Apache
%w(
  httpd
  httpd-devel
).each do |pkg|
  package pkg do
    action :install
  end
end

# Install Imagemagic and Japanese fonts
%w(
  ImageMagick
  ImageMagick-devel
  ipa-pgothic-fonts
).each do |pkg|
  package pkg do
    action :install
  end
end

#==========================================================
# Install Ruby
#==========================================================
package "git" do
  action :install
end

# Install rbenv
git "/home/vagrant/.rbenv" do
  repository "https://github.com/sstephenson/rbenv.git"
  revision   "master"
  user       "vagrant"
  group      "vagrant"
  action     :sync
end

# Install ruby-build
directory "/home/vagrant/.rbenv/plugins" do
  owner  "vagrant"
  group  "vagrant"
  action :create
end

git "/home/vagrant/.rbenv/plugins/ruby-build" do
  repository "https://github.com/sstephenson/ruby-build.git"
  revision   "master"
  user       "vagrant"
  group      "vagrant"
  action     :sync
end

# Create .bash_profile for rbenv
template ".bash_profile" do
  owner  "vagrant"
  group  "vagrant"
  mode   "0644"
  path   "/home/vagrant/.bash_profile"
  source ".bash_profile.erb"
end

template "rbenv.sh" do
  owner  "vagrant"
  group  "vagrant"
  mode   "0755"
  path   "/home/vagrant/rbenv.sh"
  source "rbenv.sh.erb"
end

# Install ruby
%w(
  gcc
  make
  openssl-devel
).each do |pkg|
  package pkg do
    action :install
  end
end

bash "rbenv install 2.2.2" do
  user  "vagrant"
  cwd   "/home/vagrant"
  code  "source rbenv.sh;rbenv install 2.2.2"
  action :run
  not_if { ::File.exists? "/home/vagrant/.rbenv/versions/2.2.2" }
end

bash "rbenv rehash" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; rbenv rehash"
  action :run
end

bash "rbenv global 2.2.2" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; rbenv global 2.2.2"
  action :run
end

# Install bundler
bash "bundler" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; gem install bundler"
  action :run
  not_if { ::File.exists? "/home/vagrant/.rbenv/shims/bundle" }
end

#==========================================================
# Install MySQL
#==========================================================
# Setup my.cnf
template "my.cnf" do
  owner  "root"
  group  "root"
  mode   "0644"
  path   "/etc/my.cnf"
  source "my.cnf.erb"
end

# Enable and start mysqld
service "mysqld" do
  action [:enable, :start]
  supports :start => true, :status => true, :restart => true, :reload => true
end

# mysql_secure_installation は一旦割愛

# Create mysql database and user for redmine
bash "create-mysql-database-and-user-for-redmine" do
  code <<-EOC
    mysql -u root -e "create database db_redmine default character set utf8;"
    mysql -u root -e "grant all on db_redmine.* to user_redmine@localhost identified by 'password123';"
    mysql -u root -e "flush privileges;"
  EOC
  only_if "mysql -u root -e 'show databases'"
end

#==========================================================
# Install Redmine
#==========================================================
# download redmine
bash "tar-open-redmine" do
  not_if "ls /var/lib/redmine/redmine-3.0.3"
  code <<-EOC
    cd /usr/local/src
    curl -O http://www.redmine.org/releases/redmine-3.0.3.tar.gz
    tar xvf redmine-3.0.3.tar.gz
    mv redmine-3.0.3 /var/lib/redmine
    chown -R vagrant:vagrant /var/lib/redmine
  EOC
end

template "database.yml" do
  owner  "vagrant"
  group  "vagrant"
  mode   "0644"
  path   "/var/lib/redmine/config/database.yml"
  source "database.yml.erb"
end

# Install gems
bash "bundler" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; cd /var/lib/redmine; bundle install --without development test"
  action :run
end

# Initialize Redmine and create database tables
bash "initialize-redmine" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; cd /var/lib/redmine; bundle exec rake generate_secret_token; RAILS_ENV=production bundle exec rake db:migrate"
  action :run
end

#==========================================================
# Install Passenger
#==========================================================
package "gcc-c++" do
  action :install
end

bash "Install passenger gem module" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; gem install passenger --no-rdoc --no-ri"
  action :run
end

bash "Install passenger apache2 module" do
  user   "vagrant"
  cwd    "/home/vagrant"
  code   "source rbenv.sh; passenger-install-apache2-module --auto"
  action :run
end

template "Setup passenger.conf" do
  owner  "root"
  group  "root"
  mode   "0644"
  path   "/etc/httpd/conf.d/passenger.conf"
  source "passenger.conf.erb"
end

bash "Change permission for apache and passenger" do
  code   "chown -R vagrant:vagrant /var/lib/redmine"
end

template "Setup passenger.conf" do
  owner  "root"
  group  "root"
  mode   "0644"
  path   "/etc/httpd/conf/httpd.conf"
  source "httpd.conf.erb"
end

# Enable and start apache
service "httpd" do
  action [:enable, :start]
  supports :start => true, :status => true, :restart => true, :reload => true
end
